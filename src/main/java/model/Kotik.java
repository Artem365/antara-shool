package model;

public class Kotik {
    private int satiety;
    private static int quantityofKotiki;
    private int prettiness;
    private String name;
    private int weight;
    private String meow;

    public Kotik(int prettiness, String name, int weight, String meow) {
        this.prettiness = prettiness;
        this.name = name;
        this.weight = weight;
        this.meow = meow;
        quantityofKotiki++;
    }

    public Kotik() {
        quantityofKotiki++;
    }

    public void setKotiki(int prettiness, String name, int weight, String meow) {
        this.prettiness = prettiness;
        this.name = name;
        this.weight = weight;
        this.meow = meow;
    }

    public boolean sleep() {
        if (satiety == 0) {
            System.out.println("Я хочу кушать");
            return false;
        }
        return true;
    }

    public boolean play() {
        if (satiety == 0) {
            System.out.println("Я хочу кушать");
            return false;
        }
        return true;
    }

    public boolean chaseMouse() {
        if (satiety == 0) {
            System.out.println("Я хочу кушать");
            return false;
        }
        return true;
    }

    public boolean walking() {
        if (satiety == 0) {
            System.out.println("Я хочу кушать");
            return false;
        }
        return true;
    }

    public boolean fighting() {
        if (satiety == 0) {
            System.out.println("Я хочу кушать");
            return false;
        }
        return true;
    }

    public void eat(int food) {
        satiety += food;
    }

    public void eat(int quantity, String food) {
        satiety += quantity * food.length();
    }

    public void eat() {
        this.eat(1, "fish");
    }

    public void liveAnotherDay() {
        int a = 1;
        int b = 5;
        for (int i = 0; i < 24; i++) {
            int n = a + (int) (Math.random() * ((b - a) + 1));
            switch (n) {
                case 1:
                    if (this.sleep()) {
                        System.out.println("Котик спит");
                    }
                    break;
                case 2:
                    if (this.play()) {
                        System.out.println("Котик играет");
                    }
                    break;
                case 3:
                    if (this.walking()) {
                        System.out.println("Котик гуляет");
                    }
                    break;
                case 4:
                    if (this.chaseMouse()) {
                        System.out.println("Котик ловит мышь");
                    }
                    break;
                case 5:
                    if (this.fighting()) {
                        System.out.println("Котик дерться (играя)");
                    }
                    break;
                default:
                    System.out.println("сломалось");
            }
        }
    }

    public int getDegreeofhungree() {
        return satiety;
    }

    public static int getQuantityofKotiki() {
        return quantityofKotiki;
    }

    public int getPrettiness() {
        return prettiness;
    }

    public String getName() {
        return name;
    }

    public int getWeight() {
        return weight;
    }

    public String getMeow() {
        return meow;
    }
}
