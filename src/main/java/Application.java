import model.Kotik;


public class Application {
    public static void main(String[] args) {
        Kotik kotik1 = new Kotik(2, "Васька", 5, "Meeeeeeowe");
        Kotik kotik2 = new Kotik();
        kotik1.eat(2, "fish");
        kotik2.eat(3, "wiskis");
        kotik2.setKotiki(1, "Борис", 7, "Mew Mew Mew");
        kotik1.liveAnotherDay();
        System.out.println(kotik1.getName() + " весит:" + kotik1.getWeight());
        System.out.println("Котики говорят одинаково " + kotik1.getMeow().equals(kotik2.getMeow()));
        System.out.println(Kotik.getQuantityofKotiki());
    }
}
